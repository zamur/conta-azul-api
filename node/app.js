/**
 * This is an example of a basic node.js script that performs
 * the Authorization Code oAuth2 flow to authenticate against
 * the ContaAzul Accounts.
 *
 * For more information, read
 * http://developers.contaazul.com
 */

var express = require('express'); // Express web server framework
var request = require('request'); // "Request" library
var querystring = require('querystring');
var cookieParser = require('cookie-parser');

var auth = require('./app/auth.js');
var product = require('./app/product.js');

var client_id = 'L81UxN6wz1I7lCL6vAhTojJZyjCg79J1'; // Your client id
var client_secret = 'uUxPZZiEA24V2ghJDSlNpJQaJJlIzrcC'; // Your secret
var redirect_uri = 'https://pipocaazul.herokuapp.com'; // Your redirect uri

var stateKey = 'contaazul_auth_state';

var app = express();

app.use(express.static(__dirname + '/public'))
   .use(cookieParser());

app.get('/login', auth.authorize);

app.get('/callback', auth.callback);

app.get('/refresh_token', auth.refreshToken);

app.get('/list_products', product.list);

app.get('/delete_product', product.delete);

var port = process.env.PORT || 8080;

console.log('Listening on ' + port);
app.listen(port);